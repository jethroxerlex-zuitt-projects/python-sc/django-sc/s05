from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class LoginForm(forms.Form):
	username = forms.CharField(label = "username", max_length = 20)
	password = forms.CharField(label = "password", max_length = 20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label = 'task_name', max_length =50)
	description = forms.CharField(label = 'description', max_length = 200)

class UpdateTaskForm(forms.Form):
	task_name = forms.CharField(label = "task_name", max_length = 50 )
	description = forms.CharField(label = "description", max_length = 200)
	status =  forms.CharField(label = "status", max_length =50)


class RegisterForm(UserCreationForm):
    class Meta:
        model=User
        fields = ['username','email','first_name','last_name','password1','password2'] 

class AddEventForm(forms.Form):
	event_name = forms.CharField(label = 'event_name', max_length =50)
	description = forms.CharField(label = 'description', max_length = 200)


class UpdateProfileForm(forms.ModelForm):
    username = forms.CharField(max_length=20,
                               required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(required=True,
                             widget=forms.TextInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(max_length=20,
                               required=True,
                               widget=forms.TextInput(attrs={'class': 'form-control'}))
    class Meta:
        model = User
        fields = ['first_name', 'last_name','password1']	        	