from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.

# The from keyword allows importing of necessary classes/modules, methods and others files needed in our application from the django.http package while the import keyword defines what we are importing from the package
from django.http import HttpResponse

# Local imports
from .models import ToDoItem
from .models import Events

# import the built in User Model
from django.contrib.auth.models import User

# to use the template created:
# from django.template import loader

# import the authenticate function
from django.contrib.auth import authenticate, login, logout

from django.forms.models import model_to_dict

from django.contrib import messages

from .forms import LoginForm, AddTaskForm, UpdateTaskForm,RegisterForm,AddEventForm,UpdateProfileForm

from django.utils import timezone

def index(request):
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	event_list = Events.objects.filter(user_id = request.user.id)
	# template = loader.get_template("index.html")
	context = {
		'todoitem_list': todoitem_list,
		'event_list':event_list,
		"user": request.user
	}
	# output = ", ".join([todoitem.task_name for todoitem in todoitem_list])
	# render(request, route_template, context)
	return render(request, "index.html", context)

def todoitem(request, todoitem_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	todoitem = get_object_or_404(ToDoItem, pk = todoitem_id)

	return render(request, "todoitem.html", model_to_dict(todoitem))


def eventitem(request, events_id):
	# response = f"You are viewing the details of {todoitem_id}"
	# return HttpResponse(response)

	eventitem_list = Events.objects.filter(user_id = request.user.id)
	eventitem = get_object_or_404(Events, pk = events_id)

	return render(request, "eventitem.html", model_to_dict(eventitem))	



# this function is responsible for registering on our application
def register(request):
    if request.method == 'GET':
        form = RegisterForm()
        return render(request, 'register.html', {'form': form})    
   
    if request.method == 'POST':
        form = RegisterForm(request.POST) 
        if form.is_valid():
            user = form.save(commit=False)
            user.username = user.username.lower()
            user.save()
            messages.success(request, 'You have singed up successfully.')
            login(request, user)
            return redirect("todolist:index")
        else:
            return render(request, 'register.html', {'form': form})	
	



	# users = User.objects.all()
	# is_user_registered = False

	# user = User()
	# user.username = "janedoe"
	# user.first_name = "jane"
	# user.last_name = "doe"
	# user.email = "jane@mail.com"
	# user.set_password("jane1234")
	# user.is_staff = False
	# user.is_active = True

	# for indiv_user in users:
	# 	if indiv_user.username == user.username:
	# 		is_user_registered = True
	# 		break

	# if is_user_registered == False:
	# 	# to save our user
	# 	user.save()

	# context = {
	# 	'is_user_registered': is_user_registered,
	# 	'first_name': user.first_name,
	# 	'last_name': user.last_name
	# }

	# return render(request, "register.html", context)



def change_password(request):
	is_user_authenticated = False

	updateuser = User.objects.filter(pk = User.id)

	context = {
		'user':request.user,
		'user_id': user_id,
		'first_name': first_name,
		'last_name': last_name,
		'password1': password1
	}
	if request.method == "POST":
		form = UpdateProfileForm(request.POST)

		if form.is_valid() == False:
			form = UpdateProfileForm()

		else:
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			password1 = form.cleaned_data['password1']

			if updateuser:

				first_name = first_name
				last_name = last_name
				password1 = password1

				updateuser.save()

				return redirect("todolist:index")
			else:
				
				context = {
					"error": True
				}
	return render(request,"change_password.html",context)
	

def login_user(request):
	context = {}

	# if this is a post request we need to process the form data
	if request.method == "POST":

		form = LoginForm(request.POST)

		if form.is_valid() == False:
			form = LoginForm()
			
		else:
			print(form)
			# cleaned_data retrieves the information from the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			user = authenticate(username = username, password = password)

			if user is not None:
				context = {
					'username' : username,
					'password' : password
				}
				login(request, user)
				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}
			
	
	return render(request, "login.html", context)








	# username = 'johndoe'
	# password = 'johndoe1234567'

	# user = authenticate(username = username, password = password)

	

def logout_user(request):
	logout(request)
	return redirect("todolist:index")


def add_task(request):
	context = {}

	if request.method == 'POST':
		form = AddTaskForm(request.POST)

		if form.is_valid() == False:
			form = AddTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']

			duplicates = ToDoItem.objects.filter(task_name = task_name, user_id = request.user.id)

			if not duplicates:
				# create an ovbject based on the ToDoItem model and saves to the record in the database

				ToDoItem.objects.create(task_name = task_name, description = description, date_created = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}

	return render(request, "add_task.html", context)


def update_task(request, todoitem_id):

	todoitem = ToDoItem.objects.filter(pk = todoitem_id)
	print(todoitem)
	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status" : todoitem[0].status
	}

	if request.method == "POST":
		form  = UpdateTaskForm (request.POST)

		if form.is_valid() == False:
			form = UpdateTaskForm()

		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:

				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()

				return redirect("todolist:viewtodoitem", todoitem_id =  todoitem[0].id)
			else:

				context = {
					"error" : True
				}

	return render(request, "update_task.html", context)

def delete_task(request, todoitem_id):

	ToDoItem.objects.filter(pk = todoitem_id).delete()

	return redirect("todolist:index")











def add_event(request):
	context = {}

	if request.method == 'POST':
		form = AddEventForm(request.POST)

		if form.is_valid() == False:
			form = AddEventForm()

		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']

			duplicates = Events.objects.filter(event_name = event_name, user_id = request.user.id)

			if not duplicates:
				# create an ovbject based on the ToDoItem model and saves to the record in the database

				Events.objects.create(event_name = event_name, description = description, event_date = timezone.now(), user_id = request.user.id)

				return redirect("todolist:index")
			else:
				context = {
					'error' : True
				}

	return render(request, "add_event.html", context)